package com.jpro.tests;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class SpinnerTest extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage stage)
    {
        BorderPane pane = new BorderPane();
        Spinner sp = new Spinner();
        sp.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 100, 50));
        sp.setEditable(true);
        sp.getEditor().requestFocus();
        //TextField sp = new TextField();

        Label l = new Label("Spinner Test");
        l.setAlignment(Pos.TOP_CENTER);
        l.setMinHeight(1000);
        pane.setTop(l);
        pane.setBottom(sp);

        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.show();
    }

}
